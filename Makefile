CFLAGS = gcc -ansi -Wall
all: add_letters.o create_frequency_table.o letter_count.o check_arg_flags.o frequency_table frequency_table.o encode_shift.o offset.o get_shift.o to_decode.o decode decode.o copyrecords copyrecords.o
frequency_table: frequency_table.o
	$(CFLAGS) -o frequency_table frequency_table.o add_letters.o create_frequency_table.o letter_count.o check_arg_flags.o
frequency_table.o: frequency_table.c
	$(CFLAGS) -c frequency_table.c
add_letters.o: add_letters.c
	$(CFLAGS) -c add_letters.c
letter_count.o: letter_count.c
	$(CFLAGS) -c letter_count.c
create_frequency_table.o: create_frequency_table.c
	$(CFLAGS) -c create_frequency_table.c
check_arg_flags.o: check_arg_flags.c
	$(CFLAGS) -c check_arg_flags.c
encode_shift.o: encode_shift.c
	$(CFLAGS) -c encode_shift.c
offset.o: offset.c
	$(CFLAGS) -c offset.c
to_decode.o: to_decode.c
	$(CFLAGS) -c to_decode.c
get_shift.o: get_shift.c
		$(CFLAGS) -c get_shift.c
decode: decode.o
	$(CFLAGS) -o decode decode.o add_letters.o create_frequency_table.o letter_count.o check_arg_flags.o encode_shift.o offset.o to_decode.o get_shift.o
decode.o: decode.c
	$(CFLAGS) -c decode.c
copyrecords: copyrecords.o
	$(CFLAGS) -o copyrecords copyrecords.o add_letters.o create_frequency_table.o letter_count.o check_arg_flags.o encode_shift.o offset.o to_decode.o get_shift.o
copyrecords.o: copyrecords.c
	$(CFLAGS) -c copyrecords.c
clean:
	rm frequency_table frequency_table.o add_letters.o create_frequency_table.o letter_count.o check_arg_flags.o encode_shift.o offset.o to_decode.o decode decode.o get_shift.o copyrecords copyrecords.o
