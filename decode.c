#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"A3.h"
int main(int argc, const char *argv[])
{
  FILE *f_in = NULL;
  FILE *f_out = NULL;
  int shift = 0;
  int loc = 0;
  char *filename;
  char *string;
  long int size = 200;
  if(check_arg_flags(argv, 'F', &loc) == 1)
  {
    if(argv[loc][2] == '\0') /*If the filename and flag are separate*/
    {
         filename = malloc(sizeof(char) * strlen(argv[loc+1]));
         strcpy(filename, argv[loc+1]);
    }
    else
    {
         int i = 2;
         filename = malloc(sizeof(char) * strlen(argv[loc])+1);
         while(argv[loc][i] != '\0')
         {
              filename[i-2] = argv[loc][i]; /*Copy character by character the filename from argv*/
              i++;
         }
    }
    if(fopen(filename, "r") != NULL)
    {
      f_in = fopen(filename, "r"); /*open file for reading*/
    }
    else
    {
      fprintf(stderr, "Error: Can't open file: %s\n", filename);
    }
    free(filename);
    fseek(f_in, 0, SEEK_END); /*Find the size of the file so that we can allocate enough memory for the string*/
    size = ftell(f_in);
    fseek(f_in, 0, SEEK_SET);
    string = malloc(sizeof(char)*size);
    fread(string, 1, size, f_in);
  }
  else
  {
    printf("Enter the text:\n");
    string = malloc(sizeof(char)*BUFFER_SIZE);
    int i = 0;
    int j = 1;
    while(feof(stdin) != 1) /*Finds the end of file and stops reading when it reaches it*/
    {
      string[i] = getchar();
      i++;
      if(i > j*BUFFER_SIZE)
      {
        j++;
        string = realloc(string, sizeof(char)*j*BUFFER_SIZE); /*Allocate an extra 200 bytes to the string*/
      }
    }
    string = realloc(string, sizeof(char)*(i+1)); /*Trim unused bytes*/
    string[i] = '\0';
  }
  if(check_arg_flags(argv, 'O', &loc) == 1)
  {
    if(argv[loc][2] == '\0')
    {
         filename = malloc(sizeof(char) * strlen(argv[loc+1]));
         strcpy(filename, argv[loc+1]);
    }
    else
    {
         int i = 2;
         filename = malloc(sizeof(char) * strlen(argv[loc]));
         while(argv[loc][i] != '\0')
         {
              filename[i-2] = argv[loc][i];
              i++;
         }
    }
    if(fopen(filename, "w") != NULL)
    {
      f_out = fopen(filename, "w");
    }
    else
    {
      fprintf(stderr, "Error: Can't open file: %s\n", filename);
    }
    free(filename);
  }
  else
  {
    f_out = stdout;
  }
  shift = encode_shift(string);
  if(check_arg_flags(argv, 's', &loc) == 1)
  {
    printf("Shift = %d\n\n", shift);
  }
  shift = to_decode(shift);
  if(check_arg_flags(argv, 'S', &loc) == 1)
  {
    printf("Shift = %d or ", shift);
    if(shift < 0)
    {
      shift += 26;
    }
    else
    {
      shift -= 26;
    }
    printf("%d\n\n", shift);
  }
  if(check_arg_flags(argv, 'x', &loc) == 1)
  {
    int *text_freq = create_frequency_table();
    add_letters(text_freq, string);
    double ef[26] = EF;
    int c, i;
    double chi = 0;
    for (i = 0; i < 26; i++)
    {
      for (c = 65; c < 91; c++)
      {
        chi += ((letter_count(string)*ef[offset(c)]) - text_freq[offset(get_shift(c, i))])*((letter_count(string)*ef[offset(c)]) - text_freq[offset(get_shift(c, i))])/(letter_count(string)*letter_count(string)*ef[offset(c)]);
      }
      printf("%f\n", chi);
      chi = 0;
    }
    printf("\n");
    free(text_freq);
  }
  if(check_arg_flags(argv, 't', &loc) == 1)
  {
    int *table = create_frequency_table();
    add_letters(table, string);
    printf("Letter Count: %d\n", letter_count(string));
    printf("Character Count: %ld\n", strlen(string));
    printf("Letter:  Count\n");
    int c;
    for(c = 'A'; c <= 'Z'; c++)
    {
        printf("   %c: %c %d\n", c, 9, table[c-'A']);
    }
    printf("\n");
    free(table);
  }
  int n = 0;
  while(string[n] != '\0')
  {
    string[n] = get_shift(string[n], shift);
    n++;
  }
  if(check_arg_flags(argv, 'O', &loc) == 0 && check_arg_flags(argv, 'n', &loc) == 0)
  {
    fprintf(f_out, "%s\n", string);
  }
  free(string);
  fflush(f_out);
  fclose(f_in);
  fclose(f_out);
  return(0);
}
