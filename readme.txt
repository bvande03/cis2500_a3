A3_q1:
  Usage: ./frequency_table -F <name of file to read from> -O <name of file to write to>
  Works with a space or no space in between the flag and the filename. If -F is not included,
  it will read up to 200 characters from stdin.
A3_q2:
  Usage: ./decode -F <name of file to read from> -O <name of file to write to> -Ssxtn
  Works no matter what order you put the flags in. If -F is missing it will read all characters
  from stdin until an EOF character is reached. -t prints the table of letters before they get
  shifted/decoded. -O option wouldn't write to a file for some reason. Many fixes for this were
  attempted, but it still wouldn't work. It seems fclose and fflush are failing for some unkown
  reason.
A3_q3
  Usage: ./copyrecords -F <name of file to read from> -O <name of file to write to>
    -D and -r flags not included. Copies a record from a file or from stdin.
