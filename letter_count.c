#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int letter_count(char *string)
{
   int i, count = 0;
   for(i = 0; i < strlen(string); i++)
   {
       if((string[i] >= 'a' && string[i] <= 'z') || (string[i] >= 'A' && string[i] <= 'Z'))
       {
            count++;
       }
   }
   return(count);
}
