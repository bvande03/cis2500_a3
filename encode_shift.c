#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"A3.h"
int encode_shift(char *string)
{
  double ef[26] = EF;
  int good_guess, c, i;
  double chi = 0;
  double smallest_chi = 200000;
  int *text_freq = create_frequency_table();
  add_letters(text_freq, string);
  for (i = 0; i < 26; i++)
  {
    for (c = 65; c < 91; c++)
    {
      chi += ((letter_count(string)*ef[offset(c)]) - text_freq[offset(get_shift(c, i))])*((letter_count(string)*ef[offset(c)]) - text_freq[offset(get_shift(c, i))])/(letter_count(string)*letter_count(string)*ef[offset(c)]);
    }
    if(chi < smallest_chi)
    {
      smallest_chi = chi;
      good_guess = i;
    }
    chi = 0;
  }
  if(smallest_chi > 0.5 && strlen(string) > 200)
  {
    return(0);
  }
  free(text_freq);
  return(good_guess);
}
