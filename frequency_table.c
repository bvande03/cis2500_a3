#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"A3.h"
int main(int argc, const char *argv[])
{
      FILE *f_in = NULL;
      int *table;
      int loc;
      char *filename;
      char *string;
      long int size = 200;
      if(check_arg_flags(argv, 'F', &loc))
      {
           printf("F\n");
           if(argv[loc][2] == '\0')
           {
                filename = malloc(sizeof(char) * strlen(argv[loc+1]));
                strcpy(filename, argv[loc+1]);
           }
           else
           {
                int i = 2;
	        filename = malloc(sizeof(char) * strlen(argv[loc]));
                while(argv[loc][i] != '\0')
                {
                     filename[i] = argv[loc][i];
                     i++;
                }
           }
           f_in = fopen(filename, "r");
      }
      if(f_in == NULL)
      {
           printf("Enter the text:\n");
           f_in = stdin;
      }
      else
      {
          fseek(f_in, 0, SEEK_END);
          size = ftell(f_in);
          fseek(f_in, 0, SEEK_SET);
      }
      string = malloc(sizeof(char) * size);
      fread(string, sizeof(char), size, f_in);
      table = create_frequency_table();
      add_letters(table, string);
      printf("Letter Count: %d\n", letter_count(string));
      printf("Character Count: %ld\n", strlen(string));
      printf("Letter:  Count\n");
      int c;
      for(c = 'A'; c <= 'Z'; c++)
      {
          printf("   %c: %c %d\n", c, 9, table[c-'A']);
      }
      if(filename != NULL)
      {
        free(filename);
      }
      free(string);
      free(table);
      fclose(f_in);
      return(0);
}
