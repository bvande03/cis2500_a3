#include<stdio.h>
#include<stdlib.h>
int check_arg_flags(char **args, char flag, int *loc)
{
     int i, j;
     /*argv terminated by null pointer. Found here: https://stackoverflow.com/questions/19481251/how-to-find-number-of-rows-in-dynamic-2d-char-array-in-c*/
     for(i = 1; args[i] != NULL; i++)
     {
        j = 0;
          while(args[i][j] != '\0')
          {
            if(flag == 'F' || flag == 'O')
            {
              if(args[i][j] == '-' && args[i][j+1] == flag)
              {
                    *loc = i;
                    return(1);
              }
            }
            else
            {
              if(args[i][j] == '-')
              {
                while(args[i][j] != '\0')
                {
                  if(args[i][j] == 'F' || args[i][j] == 'O')
                  {
                    args[i][j+1] = '\0';
                  }
                  else if(args[i][j] == flag)
                  {
                    *loc = i;
                    return(1);
                  }
                  j++;
                }
              }
            }
            j++;
          }
     }
     return(0);
}
