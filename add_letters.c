#include<stdio.h>
#include<stdlib.h>
void add_letters(int *freq_table, char *string)
{
    int i, c;
    i = 0;
    while(string[i] != '\0')
    {
        c = string[i];
        if(string[i] >= 'a' && string[i] <= 'z')
        {
             freq_table[c - 'a']++;
        }
        if(string[i] >= 'A' && string[i] <= 'Z')
        {
             freq_table[c - 'A']++;
        }
        i++;
    }
}
