#include<stdio.h>
#include<stdlib.h>
int get_shift(int c, int shift)
{
  if(shift < 26 && shift > -26)
  {
    if(shift < 0)
    {
       shift += 26;
    }
    if(c > 64 && c < 91)
    {
       c = ((c + shift - 65) % 26) + 65;
    }
    if(c > 96 && c < 123)
    {
       c = ((c + shift - 97) % 26) + 97;
    }
  }
   return(c);
}
