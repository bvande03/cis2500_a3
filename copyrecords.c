#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"A3.h"
int main(int argc, const char *argv[])
{
  FILE *f_in = NULL;
  FILE *f_out = NULL;
  int loc = 0;
  char *filename;
  char small_string[24], big_string[144];
  double double_array[24];
  int int_array[12];
  long int size = 200;
  if(check_arg_flags(argv, 'F', &loc) == 1)
  {
    if(argv[loc][2] == '\0') /*If the filename and flag are separate*/
    {
         filename = malloc(sizeof(char) * strlen(argv[loc+1]));
         strcpy(filename, argv[loc+1]);
    }
    else
    {
         int i = 2;
         filename = malloc(sizeof(char) * strlen(argv[loc])+1);
         while(argv[loc][i] != '\0')
         {
              filename[i-2] = argv[loc][i]; /*Copy character by character the filename from argv*/
              i++;
         }
    }
    if(fopen(filename, "rb") != NULL)
    {
      f_in = fopen(filename, "rb"); /*open file for reading*/
    }
    else
    {
      fprintf(stderr, "Error: Can't open file: %s\n", filename);
    }
    free(filename);
    fseek(f_in, 0, SEEK_END); /*Find the size of the file so that we can allocate enough memory for the string*/
    size = ftell(f_in);
    fseek(f_in, 0, SEEK_SET);
    int i = 0;
    while(i <= size)
    {
      fread(small_string, 1, 24, f_in);
      fread(double_array, 8, 24, f_in);
      fread(big_string, 1, 144, f_in);
      fread(int_array, 4, 12, f_in);
      i += 408; /*Size of a single record in bytes.*/
    }
  }
  else
  {
    printf("Enter the text:\n");
    string = malloc(sizeof(char)*BUFFER_SIZE);
    int i = 0;
    int j = 1;
    while(feof(stdin) != 1) /*Finds the end of file and stops reading when it reaches it*/
    {
      string[i] = getchar();
      i++;
      if(i > j*BUFFER_SIZE)
      {
        j++;
        string = realloc(string, sizeof(char)*j*BUFFER_SIZE); /*Allocate an extra 200 bytes to the string*/
      }
    }
    string = realloc(string, sizeof(char)*(i+1)); /*Trim unused bytes*/
    string[i] = '\0';
  }
  if(check_arg_flags(argv, 'O', &loc) == 1)
  {
    if(argv[loc][2] == '\0')
    {
         filename = malloc(sizeof(char) * strlen(argv[loc+1]));
         strcpy(filename, argv[loc+1]);
    }
    else
    {
         int i = 2;
         filename = malloc(sizeof(char) * strlen(argv[loc]));
         while(argv[loc][i] != '\0')
         {
              filename[i-2] = argv[loc][i];
              i++;
         }
    }
    if(fopen(filename, "wb") != NULL)
    {
      f_out = fopen(filename, "wb");
    }
    else
    {
      fprintf(stderr, "Error: Can't open file: %s\n", filename);
    }
    free(filename);
  }
  else
  {
    f_out = stdout;
  }
  fprintf(f_out, "%s\n", small_string);
  int i;
  for (i = 0; i < 24; i++)
  {
    fprintf(f_out, "%f\n", double_array[i]);
  }
  fprintf(f_out, "%s\n", big_string);
  for (i = 0 i < 12; i++)
  {
    fprintf(f_out, "%d\n", int_array[i]);
  }
  fclose(f_in);
  fclose(f_out);
  return(0);
}
